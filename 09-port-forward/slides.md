%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : NAT - Port Forwarding

<br>

```
   192.168.56.3                             192.168.90.4
          WAN          ┌─────────────┐        VM2
           │           │             │         ▲
           └───────────►   OPNsense  ├─────────┘
                       │             │
                  IN   └─────────────┘ OUT
                vboxnet0            vbownet34
                192.168.56.3        192.168.90.3
```

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Simple Port Forward

		* caution : be careful to your opnsense GUI port !!

		* install nginx and allow rules

		* Firewall > NAT > Port Forward

		* select the interface (wan > vboxnet0)

		* destination = ip of your wan

		* select a port (alias can allow you multiport)

		* redirect ip > target server (nginx) 

		* select the target port

		* enable NAT reflection

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Load Balancing Port Forward

		* create an alias with mutliple ip (targets)

		* use it in your port forward rule

		* select the pool option


