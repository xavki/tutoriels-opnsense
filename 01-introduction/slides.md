%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : introduction - what is it ?

<br>

Firewall :

	* based on freeBSD

	* history :
			* 2003 : m0n0wall
			* 2004 : fork with pfSense (Packet Filter)
			* 2014 : community leave them to create OPNsense project
			* January 2015 : first release

	* Community vs Business Edition (plugins business, book, upgrades, geoip)

Github : https://github.com/opnsense/core
Official Website : https://opnsense.org/
Documentation : https://docs.opnsense.org/intro.html

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : introduction - what is it ?

<br>

Features :

	* 802.1Q virtual lan (VLAN) : 
			* create many networks with logical division
			* useful when the number of physical interfaces is limited
			* create isolation
			* using VLAN tagging

	* Packet Filtering and Network Address Translation
			* Port forwarding
			* ip address translation

	* Quality Of Service (QoS) : prioritize packets and classify them with ipfw

	* DHCP server : manage and lease IP

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : introduction - what is it ?

<br>

Features :

	* DNS forwarder & server

	* Dynamic DNS (DDNS) : to push to an external dns (hostname)

	* IPS / IDS / IDPS : surricata usage with packet analyze and rules
			* Intrusion Detection System : analyze
			* Intrusion Prevention System : action
			* Intrusion Detection and Prevention Systems : mixte

	* Caching proxy : to cache website code static components (JS, CSS...)

	* Virtual Private Network (VPN) :
			* securize communicaiton between network
			* multiple protocol : IPsec, Wireguard, Openvpn

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : introduction - what is it ?

<br>

Features :

	* Proxy : squid

	* Report & Monitoring

	* Authentication : LDAP, radius, 2FA

	* Logs
