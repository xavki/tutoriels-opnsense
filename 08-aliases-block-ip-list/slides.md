%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

```
   192.168.56.3                             192.168.90.4
          WAN          ┌─────────────┐        VM2
           │           │             │         ▲
           └───────────►   OPNsense  ├─────────┘
                       │             │
                  IN   └─────────────┘ OUT
                vboxnet0            vbownet34
                192.168.56.3        192.168.90.3
```

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Source of malicious IP : https://github.com/firehol/blocklist-ipsets

<br>

Example : https://github.com/firehol/blocklist-ipsets/blob/master/alienvault_reputation.ipset


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Create an alias :

	* name : use an understanding name (description)

	* list = URL Table (lultiple IPs)

	* content : URLs

	* scheduling : 0 days / 1 hour

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Create rules on our target interface (wan) : block any

	* block

	* wan

	* in

	* source : our new alias

	* destination any

	* description to understand the rule

Check your rule order

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Block IP List

<br>

Clone the rule to block output trafic on the lan interface
