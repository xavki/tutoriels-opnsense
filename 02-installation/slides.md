%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : installation on virtualbox

<br>

Download OPNsense iso

https://opnsense.org/download/

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : installation on virtualbox

<br>

VirtualBox :

	* create networks

	* create virtual machine

	* add networks (private host)

	* add dvd iso

	* start


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : installation on virtualbox

<br>

OPNsense :

	* set language

	* set uefi

	* root password

	* shutdown, remove dvd and restart

	* attribute interfaces (WAN, LAN)

	* set lan interfaces

	* go into gui
