%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : VM installation & add a new interface

<br>

Last tuto :

	* install opnsense

	* create 3 networks interface wan / vboxnet0 / vboxnet1

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : VM installation & add a new interface

<br>

Add a new interface :

	* add in virtualbox new network

	* assign this interface into opnsense (gui)

	* configure this interface to use it

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : VM installation & add a new interface

<br>

Install servers : 

	* add new rules for each network

	* create VM and add iso & network

	* start and install
