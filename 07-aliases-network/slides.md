%title: OPNsense
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗███████╗███████╗███╗   ██╗███████╗███████╗
██╔═══██╗██╔══██╗████╗  ██║██╔════╝██╔════╝████╗  ██║██╔════╝██╔════╝
██║   ██║██████╔╝██╔██╗ ██║███████╗█████╗  ██╔██╗ ██║███████╗█████╗  
██║   ██║██╔═══╝ ██║╚██╗██║╚════██║██╔══╝  ██║╚██╗██║╚════██║██╔══╝  
╚██████╔╝██║     ██║ ╚████║███████║███████╗██║ ╚████║███████║███████╗
 ╚═════╝ ╚═╝     ╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Aliases

<br>

```
   192.168.56.3                             192.168.90.4
          WAN          ┌─────────────┐        VM2
           │           │             │         ▲
           └───────────►   OPNsense  ├─────────┘
                       │             │
                  IN   └─────────────┘ OUT
                vboxnet0            vbownet34
                192.168.56.3        192.168.90.3
```

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Aliases

<br>

NETWORKS

Private Network : 

	* 10.0.0.0/8, 172.16.0.0/16, 192.168.0.0/16

	* statistics

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Aliases

<br>

Rules : allow all access (outgoing)

	* vboxnetXX (LAN) : with no rules

	* internet : vboxnetXX > PASS > IN > SRC any > DST any

	* category : Accept OutGoing External Access

	* description : Accept OutGoing External Access

	* apply

	* test internet access and LAN access

-----------------------------------------------------------------------------------------------------------                                          

# OPNsense : Aliases

<br>

Rules : reject inter lan access

	* interlan : vboxnetXX > REJECT > IN > SRC any > DST PivateNetwork

	* category : Reject InterLan Access

	* description : Reject InterLan Access

	* change order > apply

	* test interlan and internet access (ping xavki.blog)


Then remove all
